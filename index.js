const request = require('request');
var express = require('express');
var bodyParser = require('body-parser');

var utils = require("./showdownScrape2");
// var _DATA = dataUtil.loadData().movies;

//for nginx implementaiotn
var path = ""

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/public', express.static('public'));


app.get(path + '/', function (req, res) {
    res.sendFile("pokemon.html", { root: __dirname });
})

app.post(path + '/api/addGame', function (req, res) {

    var logRequest = req.body.url + ".log";

    request(logRequest, { json: false }, (err, response, body) => {
        if (err) { return console.log(err); }

        // console.log(response.body.split('\n'));
        utils.scrapeLog(response.body.split('\n'))
        res.redirect(path + "/");
    });

});

app.listen(3000, function () {
    console.log('Listening on port 3000!');
});