var regexP1 = RegExp("^\\|.{0,6}\\|p1a");
var regexP2 = RegExp("^\\|.{0,6}\\|p2a");
var regexMove = RegExp("^\\|move\\|");
var regexSwitch = RegExp("^\\|switch\\|");
var regexP1Pokemon = RegExp("switch\\|p1a: (\\w*)");
var regexP2Pokemon = RegExp("switch\\|p2a: (\\w*)");

var regexP1PokemonAndMove = RegExp("move\\|p1a: ([a-zA-Z\\s-]*)\\|([a-zA-Z\\s-]*)\\|");
var regexP2PokemonAndMove = RegExp("move\\|p2a: ([a-zA-Z\\s-]*)\\|([a-zA-Z\\s-]*)\\|");

function scrapeLog(log) {
	var p1 = log.filter(line => regexP1.test(line));
	var p1Moves = p1.filter(line => regexMove.test(line));
	var p1Switches = p1.filter(line => regexSwitch.test(line));
	var p1Pokemon = Array.from(new Set(p1Switches.map(mapHelper1)));

	var p1Map = {};
	for (p in p1Pokemon) {
		p1Map[p1Pokemon[p]] = [];
	}
	// console.log(p1Map);
	for (move in p1Moves) {
		var result = regexP1PokemonAndMove.exec(p1Moves[move]);
		if (result == null) {
			console.log("oops", p1Moves[move], result);
		} else {
			p1Map[result[1]].push(result[2]);
		}
	}
	for (p in p1Map)
		p1Map[p] = Array.from(new Set(p1Map[p]));//remove duplicates



	var p2 = log.filter(line => regexP2.test(line));
	var p2Moves = p2.filter(line => regexMove.test(line));
	var p2Switches = p2.filter(line => regexSwitch.test(line));
	var p2Pokemon = Array.from(new Set(p2Switches.map(mapHelper2)));

	var p2Map = {};
	for (p in p2Pokemon) {
		p2Map[p2Pokemon[p]] = [];
	}
	// console.log(p2Map);
	for (move in p2Moves) {
		var result = regexP2PokemonAndMove.exec(p2Moves[move]);
		if (result == null) {
			console.log("oops", p2Moves[move], result);
		} else {
			p2Map[result[1]].push(result[2]);
		}
	}

	for (p in p2Map)
		p2Map[p] = Array.from(new Set(p2Map[p]));//remove duplicates
	test = p2Map;

	console.log("map1", p1Map);
	console.log("map2", p2Map);
	// console.log(p2Pokemon);
	//console.log(p2.filter(line => regexSwitch.test(line)));
	//console.log(p2);
}

function mapHelper1(str) {
	return regexP1Pokemon.exec(str)[1];
}
function mapHelper2(str) {
	return regexP2Pokemon.exec(str)[1];

}

module.exports = {
	scrapeLog: scrapeLog,
}
